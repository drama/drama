"""
===========================================
Constants Package (:mod:`drama.constants`)
===========================================

.. currentmodule:: drama.constants
  
Constants
=========

.. autosummary::
    :toctree: generated/


"""

from drama.constants.constants import *
