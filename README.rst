#############################################################
Delft Radar Modelling and performance Analysis (DRaMA)
#############################################################

Delft Radar Modelling and performance Analysis (DRaMA) is a software library for analysis of synthetic-aperture radar missions written in the Python language.
